const moodle_token = "7008ccd79805d4f9a40ab808123f1953";
const moodle_ratelimit = 5;
const filehandler_ratelimit = 5;

// TODO: Serious problems will arise if a course is renamed.
// TODO: Clean scopes!

var und = require("underscore");
var async = require("async");
var moodle = require("sync_moodle");
var mongoose = require("sync_mongo");
var filehandler = require("./filehandler_local.js")("moodlefiles", filehandler_ratelimit);

function removeAllCoursesBut(courseids, callback) {
  // 1. Find courses that have been deleted from moodle
  mongoose.getAllCoursesBut(courseids, function(err, courses) {
    if (err) {
      callback(err);
    } else {
      // 2. Try to remove the folder of the course
      async.eachLimit(courses, filehandler_ratelimit, function(course, callback) {
        filehandler.deleteCourseFolder(course.courseName, callback);
      }, function(err) {
        if (err) {
          callback(err);
        } else {
          // 3. Remove all those courses from the db
          mongoose.removeAllCoursesBut(courseids, callback);
        }
      });
    }
  });
};

function updateCourse(courseid, eachLimitCallback) {
  // 1. Get information about this course from Moodle and the local DB.
  getCourseFromMoodleAndMongoose(courseid, function(err, data) {
    if (err) {
      eachLimitCallback(err);
    } else {
      // 2. Check if the course has been modified since the last sync.
      //if (data.moodle.timemodified === data.mongoose.timemodified) {
      //  console.log(data.moodle.fullname + ": NO CHANGES");
      //  eachLimitCallback();
      //} else {
      // 3. Create course folder if it doesn't yet exists
      filehandler.createCourseFolder(data.moodle.fullname, function(err) {
        if (err) {
          callback(err);
        } else {
          // 4. Get all files of this course from Moodle.
          moodle.getCourseFiles(moodle_token, data.moodle.id, function(err, files_moodle) {
            if (err) {
              eachLimitCallback(err);
            } else {
              // 4. Join information about files from Moodle and the local DB.
              // This is a full outer join --> TODO: do something more clever!
              var files_mongoose = data.mongoose.files;
              var map = {};
              for (var i = 0; i < files_moodle.length; i++) {
                var file = files_moodle[i];
                var key = file.filepath + file.filename;
                map[key] = {};
                map[key].moodle = i;
              };
              for (var i = 0; i < files_mongoose.length; i++) {
                var file = files_mongoose[i];
                var key = file.filepath + file.filename;
                if (!map[key]) {
                  map[key] = {};
                }
                map[key].mongoose = i;
              };

              // 5. Check for each file if it has to be created, updated or deleted (locally).
              var coursename = data.moodle.fullname;
              // TODO: Scope
              async.eachLimit(und.values(map), filehandler_ratelimit, updateFile.bind(null, coursename, files_moodle, files_mongoose), function(err) {
                if (err) {
                  eachLimitCallback(err);
                } else {
                  // 6. Update 'files' array and basic information of this course.
                  // TODO: Move folder if name was changed.
                  data.mongoose.courseName = data.moodle.fullname;
                  data.mongoose.timemodified = data.moodle.timemodified;
                  // Deleted files were set to 'null' in 'updateFile'.
                  // They have to be removed from the array now.
                  data.mongoose.files = und.compact(data.mongoose.files);

                  // 7. Store updated course in DB.
                  data.mongoose.save(function(err) {
                    if (err) {
                      eachLimitCallback(err);
                    } else {
                      console.log(data.moodle.fullname + ": UPDATE SUCCESSFULL");
                      eachLimitCallback();
                    }
                  });
                }
              });
            }
          });
        }
      });
      //}
    }
  });
};

function getCourseFromMoodleAndMongoose(courseid, callback) {
  // TODO: Ratelimit
  async.parallel({
    mongoose: function(parallelCallback) {
      mongoose.getCourse(courseid, function(err, course) {
        parallelCallback(err, course);
      });
    },
    moodle: function(parallelCallback) {
      moodle.getSingleCourseByID(moodle_token, courseid, function(err, course) {
        parallelCallback(err, course);
      });
    }
  }, callback);
};

function updateFile(coursename, files_moodle, files_mongoose, mapelement, callback) {
  // TODO: Clean scoping!
  if (typeof mapelement.moodle != "undefined") { // file is on moodle
    var file_moodle = files_moodle[mapelement.moodle];
    if (typeof mapelement.mongoose != "undefined") { // file is in database
      var file_mongoose = files_mongoose[mapelement.mongoose];
      if (file_moodle.timemodified > file_mongoose.timemodified) { // file needs to be updated
        console.log("update file " + file_moodle.filepath + file_moodle.filename);
        filehandler.updateFile(coursename, file_moodle.fileurl, file_moodle.filepath, file_moodle.filename, function(err) {
          if (err) {
            callback(err);
          } else {
            file_mongoose.timemodified = file_moodle.timemodified;
            callback();
          }
        });
      } else {
        // File is up to date
        callback();
      }
    } else { // file is not in database
      console.log("create file " + file_moodle.filepath + file_moodle.filename);
      filehandler.createFile(coursename, file_moodle.fileurl, file_moodle.filepath, file_moodle.filename, function(err) {
        if (err) {
          callback(err);
        } else {
          // I know , this sucks...
          files_mongoose.push(mongoose.getNewFile({
            filename: file_moodle.filename,
            filepath: file_moodle.filepath,
            timemodified: file_moodle.timemodified
          }));
          callback();
        }
      });
    }
  } else { // file is not on moodle any more
    var file_mongoose = files_mongoose[mapelement.mongoose];
    console.log("remove file " + file_mongoose.filepath + file_mongoose.filename);
    filehandler.removeFile(coursename, file_mongoose.filepath, file_mongoose.filename, function(err) {
      if (err) {
        callback(err);
      } else {
        files_mongoose[mapelement.mongoose] = null; // array is compacted later on
        callback();
      }
    });
  }
};

function main() {
  mongoose.connect();
  mongoose.getCoursesWithSubscribers(function(err, data) {
    // 1. Aggregate subscriptions of all users to find courses with subscribers.
    if (err) {
      console.log(err);
    } else {
      var courseids = und.pluck(data, "_id");
      // 2. Remove old courses: Delete folders, then delete them from the db
      removeAllCoursesBut(courseids, function(err) {
        if (err) {
          console.log(err);
        } else {
          console.log("Cleanup done");
          // 3. Update all courses that have subscribers
          async.eachLimit(courseids, moodle_ratelimit, updateCourse, function(err) {
            if (err) {
              console.log(err);
            } else {
              console.log("All courses updated");
              mongoose.disconnect();
            }
          });
        }
      });
    }
  });
};

main();
