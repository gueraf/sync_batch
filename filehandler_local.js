var basepath_;

var path = require("path");
var fse = require("fs-extra");
var extfs = require("extfs");
var request = require("request");

module.exports = function(basepath) {
  basepath_ = basepath + "/";
  fse.mkdirpSync(basepath_);
  return exports;
};

// TODO: Handle empty courses

exports.createCourseFolder = function(coursename, callback) {
  fse.mkdirp(basepath_ + coursename, callback);
}

exports.deleteCourseFolder = function(coursename, callback) {
  fse.remove(basepath_ + coursename, callback);
}

exports.createFile = function(coursename, fileurl, filepath, filename, callback) {
  fse.mkdirp(basepath_ + coursename + "/" + filepath, function(err) {
    if (err) {
      callback(err);
    } else {
      downloadFile(fileurl, basepath_ + coursename + "/" + filepath + filename, callback);
    }
  });
};

exports.updateFile = function(coursename, fileurl, filepath, filename, callback) {
  downloadFile(fileurl, basepath_ + coursename + "/" + filepath + filename, callback);
};

exports.removeFile = function(coursename, filepath, filename, callback) {
  var file = basepath_ + coursename + "/" + filepath + filename;
  fse.exists(file, function(exists) {
    if (exists) {
      fse.unlink(file, function(err) {
        if (err) {
          callback(err);
        } else {
          removeEmpytFolders(coursename, basepath_ + coursename + "/" + filepath, callback);
        }
      });
    } else {
      console.log("Warning: File '" + file + "' did not exist.");
      removeEmpytFolders(coursename, basepath_ + coursename + "/" + filepath, callback);
    }
  });
};

function downloadFile(url, local, callback) {
  request.get({
      url: url,
      headers: {
        "User-Agent": "nodejs"
      }
    })
    .on("error", callback)
    .pipe(fse.createWriteStream(local))
    .on("error", callback)
    .on("finish", callback);
};

function removeEmpytFolders(coursename, currentpath, callback) {
  // TODO: Check for double deletion attemps
  extfs.isEmpty(currentpath, function(empty) {
    if (empty) { // current folder is empty (or doesn't exist, i.e. already deleted)
      var parent = path.normalize(currentpath + "/../");
      if (parent.indexOf(coursename) > -1) { // current folder is not course root folder
        fse.rmdir(currentpath, function(err) {
          if (!err || err.code === "ENOENT") {
            // If several files in a folder are deleted makeing it empty afterwards,
            // there is a race between multiple calls to this method. Since everything
            // is async, multiple attempts to delete the folder are made.
            // TODO: Think of a bettwer way in the future.
            removeEmpytFolders(coursename, parent, callback);
          } else {
            callback(err);
          }
        });
      } else { // current folder is course root folder
        callback();
      }
    }
  });
};
